/* eslint-disable no-undef */
$(document).ready(function() {
  var commentForm;
  var parentId;

  function form(isNew, comment) {
    $('.reply').show();

    if (commentForm) {
      commentForm.remove();
    }
    parentId = null;

    commentForm = $('form.comment').clone(true, true);

    if (isNew) {
      commentForm.find('.cancel').hide();
      commentForm.appendTo('.comment-list');
    } else {
      parentId = $(comment).closest('li').attr('id');
      $(comment).after(commentForm);
    }

    commentForm.css('display', 'flex');
  }

  // load
  form(true);

  // add form
  $('.reply').on('click', function() {
    form(false, this);
    $(this).hide();
  });

  // cancel
  $('form.comment .cancel').on('click', function(e) {
    e.preventDefault();
    commentForm.remove();
    form(true);
  });

  // publish
  $('form.comment .send').on('click', function(e) {
    e.preventDefault();

    var data = {
      post: $(".comments").attr('id'),
      body: commentForm.find('textarea').val(),
      parent: parentId
    };

    $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/comment/add'
    }).done(function(data) {
      console.log(data);
      if (!data.ok) {
        if (data.error === undefined) {
          data.error = 'Неизвестная ошибка!';
        }
        $(commentForm).prepend('<p class=\'error\'>' + data.error + '</p>');
      } else {
        var newComment = '<ul>\n' +
          '        <li class="new-comment">\n' +
          '            <div class="head">\n' +
          '                <a href="/users/' + data.login + '">' + data.login + '</a>\n' +
          '                <span class="date">' +
          '                    Только что' +
          '                </span>\n' +
          '            </div>\n' + data.body +
          '        </li>\n' +
          '</ul>';

        $(commentForm).after(newComment);
        form(true);
      }
    });
  });
});
/* eslint-enable no-undef */
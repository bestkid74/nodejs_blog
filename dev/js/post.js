/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
$(document).ready(function() {
  // clear
  $('form.post-form input, #post-body').on('focus', function() {
    $(this).closest('form').find('p.error').remove();
    $(this).closest('form').find('input, div').removeClass('error');
  });

  // publish
  $('.publish-button, .save-button').on('click', function(e) {
    e.preventDefault();

    var isDraft = $(this).attr('class').split(' ')[0] === 'save-button';

    var data = {
      title: $('#post-title').val(),
      body: $('#post-body').val(),
      isDraft: isDraft,
      postId: $('#post-id').val()
    };

    $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/post/add'
    }).done(function(data) {
      if (!data.ok) {
        $('.post-form h2').after('<p class="error">' + data.error + '</p>');
        if (data.fields) {
          data.fields.forEach(function(item) {
            $('#post-' + item).addClass('error');
          });
        }
      } else {
        if (isDraft) {
          window.location.href = '/post/edit/' + data.post.id;
        } else {
          window.location.href = '/posts/' + data.post.url;
        }
      }
    });
  });

  // upload
  $('#fileinfo').on('submit', function(e) {
    e.preventDefault();

    var formData = new FormData(this);

    $.ajax({
      type: 'post',
      url: '/upload/image',
      data: formData,
      processData: false,
      contentType: false,
      success: function(result) {
        console.log(result);
      },
      error: function(e) {
        console.log(e.message);
      }
    });
  });
});
/* eslint-enable no-unused-vars */
/* eslint-enable no-undef */
const faker = require('faker');
const models = require('./models');
const owner = '5c333a148fd202413957b03c';
const tr = require('transliter');

module.exports = async () => {
  try {
    await models.Post.deleteMany();
    Array.from({length: 20}).forEach(async () => {
      const title = faker.lorem.words(5);
      const url = `${tr.slugify(title)}-${Date.now().toString(36)}`;
      await models.Post.create({
        title,
        body: faker.lorem.words(100),
        url,
        owner
      });
    });
  } catch (e) {
    console.log(e);
  }
};
/* eslint-disable no-undef */
$(document).ready(function() {
  /**
   *  toggle login-register form
    */
  var flag = true;
  $('.switch-button').on('click', function(e) {
    e.preventDefault();

    $('form.login, form.register').find('p.error').remove();
    $('form.login, form.register').find('input').removeClass('error');
    $('form.login, form.register').find("input[type=text], input[type=password]").val("");

    if (flag) {
      flag = false;
      $('.register').show('slow');
      $('.login').hide();
    } else {
      flag = true;
      $('.login').show('slow');
      $('.register').hide();
    }
  });

  // clear
  $('form.login input, form.register input').on('focus', function() {
    $(this).closest('form').find('p.error').remove();
    $(this).closest('form').find('input').removeClass('error');
  });

  // register
  $('.register-button').on('click', function(e) {
    e.preventDefault();

    $('form.login p.error, form.register p.error').remove();
    $('form.login input, form.register input').removeClass('error');

    var data = {
      login: $('#register-login').val(),
      password: $('#register-password').val(),
      passwordConfirm: $('#register-password-confirm').val()
    };

    $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/api/auth/register'
    }).done(function(data) {
      console.log('Data: ', data);
      if (!data.ok) {
        $('.register h2').after('<p class="error">' + data.error + '</p>');
        if (data.fields) {
          data.fields.forEach(function(item) {
            $('input[name=' + item + ']').addClass('error');
          });
        }
      } else {
        window.location.reload();
      }
    });
  });

  // login
  $('.login-button').on('click', function(e) {
    e.preventDefault();

    $('form.login p.error, form.register p.error').remove();
    $('form.login input, form.register input').removeClass('error');

    var data = {
      login: $('#login-login').val(),
      password: $('#login-password').val()
    };

    $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/api/auth/login'
    }).done(function(data) {
      console.log('Data: ', data);
      if (!data.ok) {
        $('.login h2').after('<p class="error">' + data.error + '</p>');
        if (data.fields) {
          data.fields.forEach(function(item) {
            $('input[name=' + item + ']').addClass('error');
          });
        }
      } else {
        window.location.reload();
      }
    });
  });
});
/* eslint-enable no-undef */
/* eslint-disable no-undef */
$(document).ready(function() {
  /**
   *  toggle login-register form
    */
  var flag = true;
  $('.switch-button').on('click', function(e) {
    e.preventDefault();

    $('form.login, form.register').find('p.error').remove();
    $('form.login, form.register').find('input').removeClass('error');
    $('form.login, form.register').find("input[type=text], input[type=password]").val("");

    if (flag) {
      flag = false;
      $('.register').show('slow');
      $('.login').hide();
    } else {
      flag = true;
      $('.login').show('slow');
      $('.register').hide();
    }
  });

  // clear
  $('form.login input, form.register input').on('focus', function() {
    $(this).closest('form').find('p.error').remove();
    $(this).closest('form').find('input').removeClass('error');
  });

  // register
  $('.register-button').on('click', function(e) {
    e.preventDefault();

    $('form.login p.error, form.register p.error').remove();
    $('form.login input, form.register input').removeClass('error');

    var data = {
      login: $('#register-login').val(),
      password: $('#register-password').val(),
      passwordConfirm: $('#register-password-confirm').val()
    };

    $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/api/auth/register'
    }).done(function(data) {
      console.log('Data: ', data);
      if (!data.ok) {
        $('.register h2').after('<p class="error">' + data.error + '</p>');
        if (data.fields) {
          data.fields.forEach(function(item) {
            $('input[name=' + item + ']').addClass('error');
          });
        }
      } else {
        window.location.reload();
      }
    });
  });

  // login
  $('.login-button').on('click', function(e) {
    e.preventDefault();

    $('form.login p.error, form.register p.error').remove();
    $('form.login input, form.register input').removeClass('error');

    var data = {
      login: $('#login-login').val(),
      password: $('#login-password').val()
    };

    $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/api/auth/login'
    }).done(function(data) {
      console.log('Data: ', data);
      if (!data.ok) {
        $('.login h2').after('<p class="error">' + data.error + '</p>');
        if (data.fields) {
          data.fields.forEach(function(item) {
            $('input[name=' + item + ']').addClass('error');
          });
        }
      } else {
        window.location.reload();
      }
    });
  });
});
/* eslint-enable no-undef */
/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
$(document).ready(function() {
  // clear
  $('form.post-form input, #post-body').on('focus', function() {
    $(this).closest('form').find('p.error').remove();
    $(this).closest('form').find('input, div').removeClass('error');
  });

  // publish
  $('.publish-button, .save-button').on('click', function(e) {
    e.preventDefault();

    var isDraft = $(this).attr('class').split(' ')[0] === 'save-button';

    var data = {
      title: $('#post-title').val(),
      body: $('#post-body').val(),
      isDraft: isDraft,
      postId: $('#post-id').val()
    };

    $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/post/add'
    }).done(function(data) {
      if (!data.ok) {
        $('.post-form h2').after('<p class="error">' + data.error + '</p>');
        if (data.fields) {
          data.fields.forEach(function(item) {
            $('#post-' + item).addClass('error');
          });
        }
      } else {
        if (isDraft) {
          window.location.href = '/post/edit/' + data.post.id;
        } else {
          window.location.href = '/posts/' + data.post.url;
        }
      }
    });
  });

  // upload
  $('#fileinfo').on('submit', function(e) {
    e.preventDefault();

    var formData = new FormData(this);

    $.ajax({
      type: 'post',
      url: '/upload/image',
      data: formData,
      processData: false,
      contentType: false,
      success: function(result) {
        console.log(result);
      },
      error: function(e) {
        console.log(e.message);
      }
    });
  });
});
/* eslint-enable no-unused-vars */
/* eslint-enable no-undef */
/* eslint-disable no-undef */
$(document).ready(function() {
  var commentForm;
  var parentId;

  function form(isNew, comment) {
    $('.reply').show();

    if (commentForm) {
      commentForm.remove();
    }
    parentId = null;

    commentForm = $('form.comment').clone(true, true);

    if (isNew) {
      commentForm.find('.cancel').hide();
      commentForm.appendTo('.comment-list');
    } else {
      parentId = $(comment).closest('li').attr('id');
      $(comment).after(commentForm);
    }

    commentForm.css('display', 'flex');
  }

  // load
  form(true);

  // add form
  $('.reply').on('click', function() {
    form(false, this);
    $(this).hide();
  });

  // cancel
  $('form.comment .cancel').on('click', function(e) {
    e.preventDefault();
    commentForm.remove();
    form(true);
  });

  // publish
  $('form.comment .send').on('click', function(e) {
    e.preventDefault();

    var data = {
      post: $(".comments").attr('id'),
      body: commentForm.find('textarea').val(),
      parent: parentId
    };

    $.ajax({
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json',
      url: '/comment/add'
    }).done(function(data) {
      console.log(data);
      if (!data.ok) {
        if (data.error === undefined) {
          data.error = 'Неизвестная ошибка!';
        }
        $(commentForm).prepend('<p class=\'error\'>' + data.error + '</p>');
      } else {
        var newComment = '<ul>\n' +
          '        <li class="new-comment">\n' +
          '            <div class="head">\n' +
          '                <a href="/users/' + data.login + '">' + data.login + '</a>\n' +
          '                <span class="date">' +
          '                    Только что' +
          '                </span>\n' +
          '            </div>\n' + data.body +
          '        </li>\n' +
          '</ul>';

        $(commentForm).after(newComment);
        form(true);
      }
    });
  });
});
/* eslint-enable no-undef */
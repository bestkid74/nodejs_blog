const express = require('express');
const router = express.Router();
const path = require('path');
const models = require(path.join(__dirname, '../models'));

// POST for add
router.post('/add', async (req, res) => {
  const userId = req.session.userId;
  const userLogin = req.session.userLogin;

  if (!userId || !userLogin) {
    res.json({
      ok: false,
      error: 'Для данной операции авторизуйтесь!'
    });
  } else {
    const post = req.body.post;
    const body = req.body.body;
    const parent = req.body.parent;

    if (!body) {
      res.json({
        ok: false,
        error: 'Коммениарий не должен быть пустым!'
      });
    }

    try {
      if (!parent) {
        await models.Comment.create({
          body,
          post,
          owner: userId
        });

        res.json({
          ok: true,
          body,
          login: userLogin
        });
      } else {
        const parentComment = await models.Comment.findById(parent);

        if (!parentComment) {
          res.json({
            ok: false,
            error: 'Нет такого комментария!'
          });
        }

        const comment = await models.Comment.create({
          body,
          post,
          owner: userId,
          parent
        });

        const children = parentComment.children;
        children.push(comment.id);
        parentComment.children = children;
        await parentComment.save();

        res.json({
          ok: true,
          body,
          login: userLogin
        });
      }
    } catch(e) {
      res.json({
        ok: false,
        error: e.message
      });
    }
  }
});

module.exports = router;
